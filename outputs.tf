output "instances_self_link" {
  value = "${google_compute_instance.default.*.self_link}"
}

output "instance_group_self_link" {
  value = "${google_compute_instance_group.default.self_link}"
}

output "google_compute_backend_service_self_link" {
  value = "${google_compute_backend_service.default.self_link}"
}
